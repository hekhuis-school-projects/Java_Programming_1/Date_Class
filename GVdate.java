/**
 * Creates a date object with methods to:
 * -Add day
 * -Set month, day, and year
 * -Get month, day, and year 
 * -Check validity of month, day, and year
 * -Check if birthday, and if leap year
 * 
 * Kyle Hekhuis 
 * 9/25/14 - 9/29/14
 */
public class GVdate
{
    // Initialize month, day, and year variables
    private int month;
    private int day;
    private int year;
    
    // Initialize birthday constants
    private static final int BIRTHDAY_MONTH = 10;
    private static final int BIRTHDAY_DAY = 13;
    
    // Initialize month arrays
    private static final int[] JANUARY = new int[31];
    private static final int[] FEBRUARY = new int[29];
    private static final int[] MARCH = new int[31];
    private static final int[] APRIL = new int[30];
    private static final int[] MAY = new int[31];
    private static final int[] JUNE = new int[30];
    private static final int[] JULY = new int[31];
    private static final int[] AUGUST = new int[31];
    private static final int[] SEPTEMBER = new int[30];
    private static final int[] OCTOBER = new int[31];
    private static final int[] NOVEMBER = new int[30];
    private static final int[] DECEMBER = new int[31];
    private static final int[][] MONTHS = new int[][] 
                        {JANUARY, FEBRUARY, MARCH, APRIL,
                         MAY, JUNE, JULY, AUGUST, SEPTEMBER,
                         OCTOBER, NOVEMBER, DECEMBER};
                         
    // Initialize error message string 
    private static String errorDate = "Please enter a valid date.";
    
    /*
     * Create GVdate object with
     * parameters (int m, int d, int y)
     * m = month
     * d = day
     * y = year
     */ 
    public GVdate(int m, int d, int y)
    {
        // Check if passed parameters are valid
        if(isMonthValid(m) &&
           isDayValid(m, d, y) &&
           isYearValid(y))
        {
            month = m;
            day = d;
            year = y;
        }
        else
        {
            System.out.println(errorDate);
        }
        
        // Fill month arrays
        for(int i = 0; i <= 30; i++)
        {
            JANUARY[i] = i + 1;
            MARCH[i] = i + 1;
            MAY[i] = i + 1;
            JULY[i] = i + 1;
            AUGUST[i] = i + 1;
            OCTOBER[i] = i + 1;
            DECEMBER[i] = i + 1;
            if(i < 29)
            {
                FEBRUARY[i] = i + 1;
            }
            if(i < 30)
            {
                APRIL[i] = i + 1;
                JUNE[i] = i + 1;
                SEPTEMBER[i] = i + 1;
                NOVEMBER[i] = i + 1;
            }
        }
    }
    
    /*
     * Create GVdate object when passed
     * a string with a date formatted as
     * "mm/dd/yyyy"
     */
    public GVdate(String pDate)
    {
        // Parse data from passed string
        int slash1 = pDate.indexOf("/");
        int slash2 = pDate.indexOf("/", slash1 + 1);
        int slash3 = pDate.indexOf("/", slash2 + 1);
        
        int tempMonth = Integer.parseInt( pDate.substring(0, slash1) );
        int tempDay = Integer.parseInt( pDate.substring(slash1 + 1, slash2) );
        int tempYear = Integer.parseInt( pDate.substring(slash2 + 1) );
        
        // Check if passed parameters are valid
        if(isMonthValid(tempMonth) &&
           isDayValid(tempMonth, tempDay, tempYear) &&
           isYearValid(tempYear))
        {
            month = tempMonth;
            day = tempDay;
            year = tempYear;
        }
        else
        {
            System.out.println(errorDate);
        }
        
        // Fill month arrays
        for(int i = 0; i <= 30; i++)
        {
            JANUARY[i] = i + 1;
            MARCH[i] = i + 1;
            MAY[i] = i + 1;
            JULY[i] = i + 1;
            AUGUST[i] = i + 1;
            OCTOBER[i] = i + 1;
            DECEMBER[i] = i + 1;
            if(i < 29)
            {
                FEBRUARY[i] = i + 1;
            }
            if(i < 30)
            {
                APRIL[i] = i + 1;
                JUNE[i] = i + 1;
                SEPTEMBER[i] = i + 1;
                NOVEMBER[i] = i + 1;
            }
        }
    }
    
    /*
     * Set the month to the input parameter.
     */
    public void setMonth(int pMonth)
    {
        if(isMonthValid(pMonth))
        {
            month = pMonth;
        }
        else
        {
            System.out.println("Please enter a valid month.");
        }
    }
    
    /*
     * Return the month.
     */
    public int getMonth()
    {
        return month;
    }
    
    /*
     * Set the day to the input parameter.
     */
    public void setDay(int pDay)
    {   
        if(isDayValid(month, pDay, year))
        {
            day = pDay;
        }
        else
        {
            System.out.println("Please enter a valid day.");
        }
    }
    
    /*
     * Return the day.
     */
    public int getDay()
    {
        return day;
    }
    
    /*
     * Set the year to the input parameter.
     */
    public void setYear(int pYear)
    {
        if(isYearValid(pYear))
        {
            year = pYear;
        }
        else
        {
            System.out.println("Please enter a valid year.");
        }
    }
    
    /*
     * Return the year.
     */
    public int getYear()
    {
        return year;
    }
    
    /*
     * Set the instance variables.
     */
    public void setDate(int m, int d, int y)
    {
        if(isMonthValid(m) &&
           isDayValid(m, d, y) &&
           isYearValid(y))
        {
            month = m;
            day = d;
            year = y;
        }
        else
        {
            System.out.println(errorDate);
        }
    }
    
    /*
     * Return true if the date is equal to birthday constants.
     * (ignoring the year)
     */
    public boolean isMyBirthday()
    {
        if(month == BIRTHDAY_MONTH && day == BIRTHDAY_DAY)
        {
            return true;
        }
        return false;
    }
    
    /*
     * Advance the date one day.
     */
    public void nextDay()
    {
        if(month == 1)
        {
            if(day == 31)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 2)
        {
            if(isLeapYear(year) && day == 29)
            {
                day = 1;
                month++;
            }
            else if(!isLeapYear(year) && day == 28)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 3)
        {
            if(day == 31)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 4)
        {
            if(day == 30)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 5)
        {
            if(day == 31)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 6)
        {
            if(day == 30)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 7)
        {
            if(day == 31)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 8)
        {
            if(day == 31)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 9)
        {
            if(day == 30)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 10)
        {
            if(day == 31)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 11)
        {
            if(day == 30)
            {
                day = 1;
                month++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
        else if(month == 12)
        {
            if(day == 31)
            {
                day = 1;
                month = 1;
                year++;
            }
            else
            {
                day = MONTHS[month - 1][day];
            }
        }
    }
    
    /*
     * Return true if the month, day and year of the provided otherDate passed 
     * as a parameter matches the internal date - the instance variable date-
     */
    public boolean equals(GVdate otherDate)
    {
        if(month == otherDate.month &&
           day == otherDate.day &&
           year == otherDate.year)
        {
            return true;
        }
        else
            return false;
    }
    
    /*
     * Return true if passed parameter is leap year.
     */
    public boolean isLeapYear(int y) 
    {
        if(y % 4 != 0)
            return false;
        else if(y % 100 != 0)
            return true;
        else if(y % 400 != 0)
            return false;
        else
            return true;
    }
    
    /*
     * Return true if pDay is a valid day for the 
     * month and year entered as parameters. 
     */
    public boolean isDayValid (int pMonth, int pDay, int pYear)
    {
        if(pDay >= 1 && pDay <= 31 && pMonth == 1 || 
           pDay >= 1 && pDay <= 31 && pMonth == 3 || 
           pDay >= 1 && pDay <= 31 && pMonth == 5 ||
           pDay >= 1 && pDay <= 31 && pMonth == 7 ||
           pDay >= 1 && pDay <= 31 && pMonth == 8 || 
           pDay >= 1 && pDay <= 31 && pMonth == 10 ||
           pDay >= 1 && pDay <= 31 && pMonth == 12)
        {
            return true;
        }
        else if(pDay >= 1 && pDay <= 30 && pMonth == 4 || 
                pDay >= 1 && pDay <= 30 && pMonth == 6 || 
                pDay >= 1 && pDay <= 30 && pMonth == 9 ||
                pDay >= 1 && pDay <= 30 && pMonth == 11)
        {
            return true;
        }
        else if(pMonth == 2 && isLeapYear(pYear) &&
                pDay >= 1 && pDay <= 29)
        {
            return true;
        }
        else if(pMonth == 2 && !isLeapYear(pYear) &&
                pDay >= 1 && pDay <= 28)
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    
    /*
     * Return true if pMonth is a valid month.
     */
    public boolean isMonthValid(int pMonth) 
    {
        if(pMonth >= 1 && pMonth <= 12)
        {
            return true;
        }
        return false;
    }
    
    /*
     * Return true if pYear is a positive integer number.
     */
    public boolean isYearValid (int pYear)
    {
        if(pYear > 0)
        {
            return true;
        }
        return false;
    }
    
    public String toString()
    {
        return month + "/" + day + "/" + year;
    }
    
    /*
     * Enter a # 1-4 for what format you want
     * 1 = 7/10/2014
     * 2 = 07/10/2014
     * 3 = Jul 10, 2014
     * 4 = July 10, 2014
     */
    public String toString(int format)
    {
        // Initialize string array with names of months
        String[] monthNames = { "January", "February", "March",
                                "April", "May", "June", "July",
                                "August", "September", "October",
                                "November", "December" };
                                
        if(format == 1)
        {
            return month + "/" + day + "/" + year;
        }   
        else if(format == 2)
        {
               return String.format( "%02d", month ) + "/" + String.format( "%02d", day ) + "/" + year;
        }
        else if(format == 3)
        {
            return monthNames[month - 1].substring(0, 3) + " " + day + ", " + year;
        }
        else if(format == 4)
        {
            return monthNames[month - 1] + " " + day + ", " + year;
        }
        return null;
    }
}
