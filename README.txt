Date Class
Language: Java
Authors: Kyle Hekhuis (https://gitlab.com/hekhuisk)
Class: CIS 163 - Java Programming 1
Semester / Year: Fall 2014

This class creates a date object with ability to set the date,
get the date, check if the date is valid, check if the date is in a 
eap year, or if the date is the birthday defined in constants.

Usage:
Implement the class as you wish.