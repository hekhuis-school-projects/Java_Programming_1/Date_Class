
/**
 * Main method to test the GVdate class and its methods.
 * 
 * Kyle Hekhuis 
 * 9/29/14
 */
public class GVdate_Tester
{
    public static void main(String [] args)
    {
        System.out.println("Fall 2014 – CIS162 Project 2 – Kyle Hekhuis");
        System.out.println("===========================================");
        
        GVdate date1 = new GVdate(4,10,2014);
        GVdate date2 = new GVdate("4/10/2014");
        
        System.out.println("date1: " + date1);
        System.out.println("date2: " + date2);
        System.out.println("Are date1 and date2 equal? " + date1.equals(date2));
        System.out.println("Today: " + date1);
        date1.nextDay();
        System.out.println("Tomorrow: " + date1);
        date1.nextDay();
        System.out.println("Next Day: " + date1);
        
        System.out.println("");
        System.out.println("Testing set methods");
        System.out.println("===================");
        date1.setDay(2);
        date1.setMonth(3);
        date1.setYear(1934);
        System.out.println("date1: " + date1);
        date2.setDate(12,27,1999);
        System.out.println("date2: " + date2);
        System.out.println("Are date1 and date2 equal? " + date1.equals(date2));
        
        System.out.println("");
        System.out.println("Testing leap year");
        System.out.println("=================");        
        System.out.println("Is 2014 leap year? " + date1.isLeapYear(2014));
        System.out.println("Is 2000 leap year? " + date1.isLeapYear(2000));
        System.out.println("Is 1900 leap year? " + date1.isLeapYear(1900));
        System.out.println("Is 1994 leap year? " + date1.isLeapYear(1994));
        
        System.out.println("");
        System.out.println("Testing birthday");
        System.out.println("date1? " + date1.isMyBirthday());
        System.out.println("date2? " + date2.isMyBirthday());
        
        System.out.println("");
        System.out.println("Testing invalid parameters");
        System.out.println("==========================");
        date1.setDate(2,29,1998);
        
        System.out.println("");
        System.out.println("Testing string formats");
        System.out.println("======================");
        System.out.println("Format 1: " + date1.toString(1));
        System.out.println("Format 2: " + date1.toString(2));
        System.out.println("Format 3: " + date1.toString(3));
        System.out.println("Format 4: " + date1.toString(4));
    }
}
